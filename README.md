# Graph color vertex
### Description
This project start server and view colored graph
### Functional
All site locate on `http://localhost:8000/graphs/` (by default)
- __Create vertex__
  ![alt text](./md_photoes/create.png)
- __Delete vertex__
  ![alt text](./md_photoes/delete.png)
- __Create link__
  ![alt text](./md_photoes/create_link.png)
- __Delete link__
  ![alt text](./md_photoes/delete_link.png)
  
### Start
#### Linux (in Terminal)
```bash
bash ./start_linux.sh
```
#### Windows (in PowerShell)
```PowerShell
./start_windows.ps1
```
### System requirements
- Python 3.9.1
- pip 20.2.2