from random import uniform


def change_graph_colors(graphs: list, links: list):
    hashmap = dict()
    for i in graphs:
        hashmap[i['id']] = i
        if 'color' in i:
            del i['color']
    if len(hashmap.keys()) != 0:
        colors = []
        for a in hashmap.keys():
            if 'color' not in hashmap[a]:
                stack = []

                stack.append(a)
                while len(stack) != 0:
                    current_element = stack[0]
                    stack.remove(current_element)
                    for el in hashmap[current_element]['links']:
                        if el not in hashmap:
                            hashmap[current_element]['links'].remove(el)
                            if el in stack:
                                stack.remove(el)
                        else:
                            if el not in stack and 'color' not in hashmap[el]:
                                stack.append(el)
                    for color in colors:
                        flag = True
                        for el in hashmap[current_element]['links']:
                            if 'color' in hashmap[el] and hashmap[el]['color'] == color:
                                flag = False
                                break
                        if flag:
                            hashmap[current_element]['color'] = color
                    if 'color' not in hashmap[current_element]:
                        r = int(uniform(1, 2**32) % 256)
                        g = int(uniform(1, 2**32) % 256)
                        b = int(uniform(1, 2**32) % 256)
                        new_color = {
                            'r': r,
                            'g': g,
                            'b': b
                        }
                        hashmap[current_element]['color'] = new_color
                        colors.append(new_color)

        return graphs, links
    else:
        return [], []

