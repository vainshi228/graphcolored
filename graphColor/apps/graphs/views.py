import json

from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime

from .algorithms import change_graph_colors

def index(request):
    if request.method == 'GET':
        graphs = []
        links = []
        return render(request, 'graphs/main_graph.html', {
            'graphs': graphs,
            'links': links
        })
    elif request.method == 'POST':
        print(f'[{datetime.now()}] {request.method} {request.body}')
        body = json.loads(request.body)
        if 'graph' in body['newElement']:
            last_id = 0
            for graph in body['graphs']:
                if last_id < int(graph['id']):
                    last_id = int(graph['id'])
            graph = body['newElement']['graph']
            body['graphs'].append({
                'id': str(last_id + 1),
                'position': {
                    'x': graph['x'],
                    'y': graph['y']
                },
                'links': []
            })
        else:
            link = body['newElement']['link']
            body['links'].append(link)
            for el in body['graphs']:
                if el['id'] == link['from_graph']:
                    el['links'].append(link['to_graph'])
                    link['from_position'] = el['position']
                if el['id'] == link['to_graph']:
                    el['links'].append(link['from_graph'])
                    link['to_position'] = el['position']
        (graphs, links) = change_graph_colors(body['graphs'], body['links'])
        return HttpResponse(json.dumps({'graphs': graphs, 'links': links}), content_type="application/json")
    elif request.method == 'DELETE':
        print(f'[{datetime.now()}] {request.method} {request.body}')
        body = json.loads(request.body)
        if 'graphId' in body:
            list_ghost_links = []
            links = body['links']
            for i in range(len(links)):
                if str(links[i]['to_graph']) == str(body['graphId']) or str(links[i]['from_graph']) == str(body['graphId']):
                    list_ghost_links.append(i)
            k = 0
            for i in list_ghost_links:
                links.remove(links[i - k])
                k += 1
            for graph in body['graphs']:
                if str(graph['id']) == str(body['graphId']):
                    body['graphs'].remove(graph)
                if str(body['graphId']) in graph['links']:
                    graph['links'].remove(str(body['graphId']))
            (graphs, links) = change_graph_colors(body['graphs'], body['links'])
            return HttpResponse(json.dumps({'graphs': graphs, 'links': links}), content_type="application/json")
        elif 'link' in body:
            del_link = body['link']
            for link in body['links']:
                if link['from_graph'] in del_link and link['to_graph'] in del_link:
                    body['links'].remove(link)
                    break
            for graph in body['graphs']:
                if str(graph['id']) in del_link:
                    for i in del_link:
                        if str(graph['id']) != i:
                            graph['links'].remove(i)
            (graphs, links) = change_graph_colors(body['graphs'], body['links'])
            return HttpResponse(json.dumps({'graphs': graphs, 'links': links}), content_type="application/json")
    resp = HttpResponse("Internal server error", content_type="application/json")
    resp.status_code = 500
    return resp